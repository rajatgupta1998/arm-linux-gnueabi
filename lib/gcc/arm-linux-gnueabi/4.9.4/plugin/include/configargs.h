/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure --disable-multilib --disable-werror --enable-languages=c,c++ --target=arm-linux-gnueabi --prefix=/home/rajatgupta1998/android/aosp/build-tools-gcc/arm-linux-gnueabi";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "tls", "gnu" } };
